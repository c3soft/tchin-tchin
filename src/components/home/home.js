import React from 'react';
import imgServer from './background_home.svg';
import imgMocktail from './mocktail.png';
import imgCocktail from './cocktail.png';
import './home.css';

export default function home() {
  return (
    <div>
      <h1>Tchin Tchin</h1>
      <img src={imgServer} alt="image serveur maitre d'hotel"></img>
      <div className="bt_choiseM">
        <img src={imgMocktail} alt="Bouton selection Mocktail"></img>
      </div>
      <div className="bt_choiseC">
        <img src={imgCocktail} alt="Bouton selection Cocktail"></img>
      </div>
    </div>
  );
}
